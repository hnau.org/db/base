package org.hnau.db.base.sql


abstract class ParamsInjector<PR> {

    protected abstract fun addParam(setParam: PR.() -> Unit)

    fun inject(setParam: PR.() -> Unit) =
            "?".also { addParam(setParam) }

}