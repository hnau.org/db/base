package org.hnau.db.base.sql


data class ParametrizedSql<PR>(
        val sql: String,
        val params: Iterable<PR.() -> Unit>
) {

    companion object {}

}