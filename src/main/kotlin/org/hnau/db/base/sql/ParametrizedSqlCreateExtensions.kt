package org.hnau.db.base.sql

inline fun <PR> ParametrizedSql(
        build: ParamsInjector<PR>.() -> String
): ParametrizedSql<PR> {

    val paramsInjector = object : ParamsInjector<PR>() {

        private val paramsInner = ArrayList<PR.() -> Unit>()
        val params: Collection<PR.() -> Unit> get() = paramsInner

        override fun addParam(setParam: PR.() -> Unit) {
            paramsInner.add(setParam)
        }

        override fun toString() =
                "ParamsInjector(params=$params)"

    }

    val sql = paramsInjector.build()
    return ParametrizedSql<PR>(
            sql = sql,
            params = paramsInjector.params
    )
}