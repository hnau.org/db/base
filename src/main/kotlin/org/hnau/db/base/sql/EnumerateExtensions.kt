package org.hnau.db.base.sql

import org.hnau.base.data.mapper.Mapper


private const val separator = ","

fun Iterable<String>.enumerate() =
        joinToString(separator)

inline fun <T> Iterable<T>.enumerate(transform: (T) -> String) =
        map(transform).enumerate()

fun Array<String>.enumerate() =
        joinToString(separator)

inline fun <T> Array<T>.enumerate(transform: (T) -> String) =
        map(transform).enumerate()

fun ByteArray.enumerate() =
        joinToString(separator)

inline fun ByteArray.enumerate(transform: (Byte) -> String) =
        map(transform).enumerate()

fun ShortArray.enumerate() =
        joinToString(separator)

inline fun ShortArray.enumerate(transform: (Short) -> String) =
        map(transform).enumerate()

fun CharArray.enumerate() =
        joinToString(separator)

inline fun CharArray.enumerate(transform: (Char) -> String) =
        map(transform).enumerate()

fun IntArray.enumerate() =
        joinToString(separator)

inline fun IntArray.enumerate(transform: (Int) -> String) =
        map(transform).enumerate()

fun LongArray.enumerate() =
        joinToString(separator)

inline fun LongArray.enumerate(transform: (Long) -> String) =
        map(transform).enumerate()

fun FloatArray.enumerate() =
        joinToString(separator)

inline fun FloatArray.enumerate(transform: (Float) -> String) =
        map(transform).enumerate()

fun DoubleArray.enumerate() =
        joinToString(separator)

inline fun DoubleArray.enumerate(transform: (Double) -> String) =
        map(transform).enumerate()

fun BooleanArray.enumerate() =
        joinToString(separator)

inline fun BooleanArray.enumerate(transform: (Boolean) -> String) =
        map(transform).enumerate()

inline fun <reified E : Enum<*>> enumerate(crossinline transform: E.() -> String = { "'$name'" }) =
        E::class.java.enumConstants.enumerate(transform)

