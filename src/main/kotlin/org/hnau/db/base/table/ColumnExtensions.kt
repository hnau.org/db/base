package org.hnau.db.base.table

import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.checkNullable


private fun Column.buildCustomName(
        tableColumnSeparation: String
) = table.checkNullable(
        ifNull = { selfName },
        ifNotNull = { table -> "${table.name}$tableColumnSeparation$selfName" }
)

val Column.fullName
    get() = buildCustomName(".")

val Column.alias
    get() = buildCustomName("_column_")

val Column.fullNameAsAlias
    get() = (fullName == alias).checkBoolean(
            ifTrue = { fullName },
            ifFalse = { "$fullName AS $alias" }
    )