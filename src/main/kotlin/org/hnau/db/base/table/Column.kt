package org.hnau.db.base.table


data class Column(
        val table: Table?,
        val selfName: String
) {

    override fun toString() = selfName

}