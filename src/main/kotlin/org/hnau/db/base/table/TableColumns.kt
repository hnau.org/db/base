package org.hnau.db.base.table


abstract class TableColumns(
        private val table: Table
) {

    protected fun column(selfName: String) =
            Column(table, selfName)

}